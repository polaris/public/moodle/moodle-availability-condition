<?php
/**
 * Version info.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version = 2022112801;
$plugin->requires = 2022111800;
$plugin->component = 'availability_analyticsengineresult';
