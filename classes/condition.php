<?php
/**
 * User profile field condition.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace availability_analyticsengineresult;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

defined('MOODLE_INTERNAL') || die();

/**
 * User profile field condition.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class condition extends \core_availability\condition {
    /** @var string Operator: field contains value */
    const OP_CONTAINS = 'contains';

    /** @var string Operator: field does not contain value */
    const OP_DOES_NOT_CONTAIN = 'doesnotcontain';

    /** @var string Operator: field equals value */
    const OP_IS_EQUAL_TO = 'isequalto';

    /** @var string Operator: field starts with value */
    const OP_STARTS_WITH = 'startswith';

    /** @var string Operator: field ends with value */
    const OP_ENDS_WITH = 'endswith';

    /** @var string Operator: field is empty */
    const OP_IS_EMPTY = 'isempty';

    /** @var string Operator: field is not empty */
    const OP_IS_NOT_EMPTY = 'isnotempty';

    /** @var array|null Array of custom profile fields (static cache within request) */
    protected static $customprofilefields = null;

    /** @var string Field name (for standard fields) or '' if custom field */
    protected $standardfield = '';

    /** @var int Field name (for custom fields) or '' if standard field */
    protected $customfield = '';

    /** @var string Operator type (OP_xx constant) */
    protected $operator;

    /** @var string Expected value for field */
    protected $value = '';
    protected $field = '';

    /** @var string base value of polaris backend */
    private $baseUrl = "https://polaris.ruhr-uni-bochum.de";


    private $application_token = "27dd35314d73c7618945af9ac3152ecfcb9d1992ead2bdc562409d01ed456f6d"; //TODO

    private $debug = true;

    /**
     * Constructor.
     *
     * @param \stdClass $structure Data structure from JSON decode
     * @throws \coding_exception If invalid data structure.
     */
    public function __construct($structure) {
        // Get operator.
        if (isset($structure->op) && in_array($structure->op, array(self::OP_CONTAINS,
                self::OP_DOES_NOT_CONTAIN, self::OP_IS_EQUAL_TO, self::OP_STARTS_WITH,
                self::OP_ENDS_WITH, self::OP_IS_EMPTY, self::OP_IS_NOT_EMPTY), true)) {
            $this->operator = $structure->op;
        } else {
            throw new \coding_exception('Missing or invalid ->op for analytics condition');
        }

        // For operators other than the empty/not empty ones, require value.
        switch($this->operator) {
            case self::OP_IS_EMPTY:
            case self::OP_IS_NOT_EMPTY:
                if (isset($structure->v)) {
                    throw new \coding_exception('Unexpected ->v for non-value operator');
                }
                break;
            default:
                if (isset($structure->v) && is_string($structure->v)) {
                    $this->value = $structure->v;
                } else {
                    throw new \coding_exception('Missing or invalid ->v for analytics condition');
                }
                break;
        }

        // Get field type.
        if (!property_exists($structure, 'field')) {            
            throw new \coding_exception('Missing ->field for analytics condition');          
        }
        $this->field = $structure->field;
    }

    public function save() {
        $result = (object)array('type' => 'analyticsengineresult', 'op' => $this->operator);

        $result->field = $this->customfield;
      
        switch($this->operator) {
            case self::OP_IS_EMPTY:
            case self::OP_IS_NOT_EMPTY:
                break;
            default:
                $result->v = $this->value;
                break;
        }
        return $result;
    }

    /**
     * Returns a JSON object which corresponds to a condition of this type.
     *
     * Intended for unit testing, as normally the JSON values are constructed
     * by JavaScript code.
     *
     * @param bool $customfield True if this is a custom field
     * @param string $fieldname Field name
     * @param string $operator Operator name (OP_xx constant)
     * @param string|null $value Value (not required for some operator types)
     * @return stdClass Object representing condition
     */
    public static function get_json($customfield, $fieldname, $operator, $value = null) {
        $result = (object)array('type' => 'profile', 'op' => $operator);
        if ($customfield) {
            $result->cf = $fieldname;
        } else {
            $result->sf = $fieldname;
        }
        switch ($operator) {
            case self::OP_IS_EMPTY:
            case self::OP_IS_NOT_EMPTY:
                break;
            default:
                if (is_null($value)) {
                    throw new \coding_exception('Operator requires value');
                }
                $result->v = $value;
                break;
        }
        return $result;
    }

    public function is_available($not, \core_availability\info $info, $grabthelot, $userid) {
        global $USER, $DB, $CFG;
        $iscurrentuser = $USER->id == $userid;
        if (isguestuser($userid) || ($iscurrentuser && !isloggedin())) {
            // Must be logged in and can't be the guest.
            return false;
        }
    //    try {
        // $email = $DB->get_field('user', "email", array('id' => $userid), MUST_EXIST);
        $email = "user_".$USER->username."@ruhr-uni-bochum.de"; // Bochum Variante 
        $engine = explode(".",$this->field)[0];
        if($this->debug)
            print_r("Engine:".$engine);
        $visualisationToken = $this->fetchVisualisationToken([],$email,[$engine]);
        if($this->debug)
            print_r("vs-token:".$visualisationToken);
        $analytics_engine_result = $this->fetchDashboardResult($visualisationToken,true);
        if($this->debug) {
            print_r("analytics-engine-result:");
            print_r($analytics_engine_result);
        }
        if($analytics_engine_result != null && array_key_exists($this->field,$analytics_engine_result))
        {
            $value = $analytics_engine_result[$this->field];
            return self::is_field_condition_met($this->operator,$value,$this->value);
        }
        return false;
  //      } catch(\Exception $exception) {}
        return false;
    }
    
    public function get_description($full, $not, \core_availability\info $info) {
        return "Polaris / die Lernanalyse gibt diese Inhalte frei, in dem die Bedingung erfüllt ist.";
    }

    protected function get_debug_string() {
        if ($this->customfield) {
            $out = '*' . $this->customfield;
        } else {
            $out = $this->standardfield;
        }
        $out .= ' ' . $this->operator;
        switch($this->operator) {
            case self::OP_IS_EMPTY:
            case self::OP_IS_NOT_EMPTY:
                break;
            default:
                $out .= ' ' . $this->value;
                break;
        }
        return $out;
    }

    /**
     * Returns true if a field meets the required conditions, false otherwise.
     *
     * @param string $operator the requirement/condition
     * @param string $uservalue the user's value
     * @param string $value the value required
     * @return boolean True if conditions are met
     */
    protected static function is_field_condition_met($operator, $uservalue, $value) {
        if ($uservalue === false) {
            // If the user value is false this is an instant fail.
            // All user values come from the database as either data or the default.
            // They will always be a string.
            return false;
        }
        $fieldconditionmet = true;
        // Just to be doubly sure it is a string.
        $uservalue = (string)$uservalue;
        switch($operator) {
            case self::OP_CONTAINS:
                $pos = strpos($uservalue, $value);
                if ($pos === false) {
                    $fieldconditionmet = false;
                }
                break;
            case self::OP_DOES_NOT_CONTAIN:
                if (!empty($value)) {
                    $pos = strpos($uservalue, $value);
                    if ($pos !== false) {
                        $fieldconditionmet = false;
                    }
                }
                break;
            case self::OP_IS_EQUAL_TO:
                if ($value !== $uservalue) {
                    $fieldconditionmet = false;
                }
                break;
            case self::OP_STARTS_WITH:
                $length = strlen($value);
                if ((substr($uservalue, 0, $length) !== $value)) {
                    $fieldconditionmet = false;
                }
                break;
            case self::OP_ENDS_WITH:
                $length = strlen($value);
                $start = $length * -1;
                if (substr($uservalue, $start) !== $value) {
                    $fieldconditionmet = false;
                }
                break;
            case self::OP_IS_EMPTY:
                if (!empty($uservalue)) {
                    $fieldconditionmet = false;
                }
                break;
            case self::OP_IS_NOT_EMPTY:
                if (empty($uservalue)) {
                    $fieldconditionmet = false;
                }
                break;
        }
        return $fieldconditionmet;
    }
   

    public function is_applied_to_user_lists() {
        // Profile conditions are assumed to be 'permanent', so they affect the
        // display of user lists for activities.
        return true;
    } 

    public function fetchVisualisationToken($engines = [], $context_id = null, $engines_with_context = [])
    {
        if($context_id != null) {
            $response = $this->postRequest("/api/v1/provider/visualization-tokens/create",
                json_encode(["engines" => $engines, "context_id" => $context_id, "engines_with_context" => $engines_with_context, "application_token" => $this->application_token]));
        } else {
            $response = $this->postRequest("/api/v1/provider/visualization-tokens/create",
                json_encode(["engines" => $engines, "application_token" => $this->application_token]));
        }
        if($response != null)
            return $response->token;
        return null;
    }

    public function fetchDashboardResult($visualisation_token, $asDot = false)
    {
        $analytics_engine_result = $this->getRequest("/api/v1/provider/result",$visualisation_token);
        if($analytics_engine_result == null)
            return null;
        if($asDot)
        {
            $ritit = new RecursiveIteratorIterator(new RecursiveArrayIterator($analytics_engine_result));
            $result = array();
            foreach ($ritit as $leafValue) {
                $keys = array();
                foreach (range(0, $ritit->getDepth()) as $depth) {
                    $keys[] = $ritit->getSubIterator($depth)->key();
                }
                $result[ join('.', $keys) ] = $leafValue;
            }
            return $result;
        }
        return $analytics_engine_result;
    }

    function getRequest($url,$visualisation_token)
    {
        $curlHandle = curl_init($this->baseUrl.$url);
        $header = array();
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: Basic '.$visualisation_token;

        curl_setopt($curlHandle, CURLOPT_HTTPHEADER,$header);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

        $curlResponse = curl_exec($curlHandle);
        if (curl_errno($curlHandle)) {
            $error_msg = curl_error($curlHandle);
        }
        if (isset($error_msg) && $this->debug) {
           print_r($curlResponse);
           print_r($error_msg);
        }
        curl_close($curlHandle);
        return json_decode($curlResponse);
    }

    function postRequest($url,$data)
    {
        $curlHandle = curl_init($this->baseUrl.$url);
        $header = array();
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: Basic '.$this->application_token;


        curl_setopt($curlHandle, CURLOPT_HTTPHEADER,$header);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

        $curlResponse = curl_exec($curlHandle);
        if (curl_errno($curlHandle)) {
            $error_msg = curl_error($curlHandle);
        }
        if (isset($error_msg) && $this->debug) {
           print_r($curlResponse);
           print_r($error_msg);
        }
        curl_close($curlHandle);
        return json_decode($curlResponse);
    }
   
}
