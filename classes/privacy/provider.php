<?php
/**
 * Privacy Subsystem implementation for availability_analyticsengineresult.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace availability_analyticsengineresult\privacy;

defined('MOODLE_INTERNAL') || die();

/**
 * Privacy Subsystem for availability_analyticsengineresult implementing null_provider.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements \core_privacy\local\metadata\null_provider {

    /**
     * Get the language string identifier with the component's language
     * file to explain why this plugin stores no data.
     *
     * @return  string
     */
    public static function get_reason() : string {
        return 'privacy:metadata';
    }
}