<?php
/**
 * Version info.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace availability_analyticsengineresult;

defined('MOODLE_INTERNAL') || die();

/**
 * Front-end class.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class frontend extends \core_availability\frontend {
    protected function get_javascript_strings() {
        return array('op_contains', 'op_doesnotcontain', 'op_endswith', 'op_isempty',
                'op_isequalto', 'op_isnotempty', 'op_startswith', 'conditiontitle',
                'label_operator', 'label_value');
    }

    protected function get_javascript_init_params($course, \cm_info $cm = null,
            \section_info $section = null) {

    }
}
