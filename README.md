# Moodle availability condition

The plugin introduces a new condition to Moodle, enabling the hiding of blocks or other elements from users if a specific statement is not present in Polaris' result store. This functionality provides the opportunity to display learning content, courses, or other information only when a specific score is calculated by the Polaris engine.

## Requirements 
- curl
- Firewall / Proxy: Create connection to the rights engine with specific headers 

## Installation

You can install the plugin by downloading the source as a zip file and then uploading the archive in Moodle. 

## Configuration and Usage
