YUI.add('moodle-availability_analyticsengineresult-form', function (Y, NAME) {

/**
 * JavaScript for form editing profile conditions.
 *
 * @module moodle-availability_analyticsengineresult-form
 */
M.availability_analyticsengineresult = M.availability_analyticsengineresult || {};

/**
 * @class M.availability_analyticsengineresult.form
 * @extends M.core_availability.plugin
 */
M.availability_analyticsengineresult.form = Y.Object(M.core_availability.plugin);

/**
 * Groupings available for selection (alphabetical order).
 *
 * @property results
 * @type Array
 */
M.availability_analyticsengineresult.form.profiles = null;

/**
 * Initialises this plugin.
 *
 * @method initInner
 * @param {Array} standardFields Array of objects with .field, .display
 * @param {Array} customFields Array of objects with .field, .display
 */
M.availability_analyticsengineresult.form.initInner = function(standardFields, customFields) {
    this.standardFields = standardFields;
    this.customFields = customFields;
};

M.availability_analyticsengineresult.form.getNode = function(json) {
    // Create HTML structure.
    var html = '<h3>' + M.util.get_string('conditiontitle', 'availability_analyticsengineresult')
             +'</h3><span class="availability-group"><label><span class="pr-3">' +
            M.util.get_string('conditiontitle', 'availability_analyticsengineresult') + '</span> ' +
            ' <label><span class="accesshide">' 
            + M.util.get_string('label_field', 'availability_analyticsengineresult') +
                    '</span><input name="field" type="text" class="form-control" style="width: 10em" title="' +
                    M.util.get_string('label_field', 'availability_analyticsengineresult') + '"/></label>' +
                    '<label><span class="accesshide">' 
    + M.util.get_string('label_operator', 'availability_analyticsengineresult') +
            ' </span><select name="op" title="' + M.util.get_string('label_operator', 'availability_analyticsengineresult') + '"' +
                     ' class="custom-select">';

    var operators = ['isequalto', 'contains', 'doesnotcontain', 'startswith', 'endswith',
            'isempty', 'isnotempty'];
    for (i = 0; i < operators.length; i++) {
        html += '<option value="' + operators[i] + '">' +
                M.util.get_string('op_' + operators[i], 'availability_analyticsengineresult') + '</option>';
    }
    html += '</select></label> <label><span class="accesshide">' 
    + M.util.get_string('label_value', 'availability_analyticsengineresult') +
            '</span><input name="value" type="text" class="form-control" style="width: 10em" title="' +
            M.util.get_string('label_value', 'availability_analyticsengineresult') + '"/></label></span>';
    var node = Y.Node.create('<span class="form-inline">' + html + '</span>');

    // Set initial values if specified.
    if (json.field !== undefined) {
        node.one('input[name=field]').set('value', json.field);
    }
    if (json.op !== undefined &&
            node.one('select[name=op] > option[value=' + json.op + ']')) {
        node.one('select[name=op]').set('value', json.op);
        if (json.op === 'isempty' || json.op === 'isnotempty') {
            node.one('input[name=value]').set('disabled', true);
        }
    }
    if (json.v !== undefined) {
        node.one('input[name=value]').set('value', json.v);
    }

    // Add event handlers (first time only).
    if (!M.availability_analyticsengineresult.form.addedEvents) {
        M.availability_analyticsengineresult.form.addedEvents = true;
        var updateForm = function(input) {
            var ancestorNode = input.ancestor('span.availability_analyticsengineresult');
            var op = ancestorNode.one('select[name=op]');
            var novalue = (op.get('value') === 'isempty' || op.get('value') === 'isnotempty');
            ancestorNode.one('input[name=value]').set('disabled', novalue);
            M.core_availability.form.update();
        };
        var root = Y.one('.availability-field');
        root.delegate('change', function() {
             updateForm(this);
        }, '.availability_analyticsengineresult select');
        root.delegate('change', function() {
             updateForm(this);
        }, '.availability_analyticsengineresult input[name=value]');
    }

    return node;
};

M.availability_analyticsengineresult.form.fillValue = function(value, node) {
    // Set field.
    var field = node.one('input[name=field]').get('value');
    value.field = field;

    // Operator and value
    value.op = node.one('select[name=op]').get('value');
    var valueNode = node.one('input[name=value]');
    if (!valueNode.get('disabled')) {
        value.v = valueNode.get('value');
    }
};

M.availability_analyticsengineresult.form.fillErrors = function(errors, node) {
    var value = {};
    this.fillValue(value, node);

    // Check profile item id.
    if (value.sf === undefined && value.field === undefined) {
        errors.push('availability_analyticsengineresult:error_selectfield');
    }
    if (value.v !== undefined && /^\s*$/.test(value.v)) {
        errors.push('availability_analyticsengineresult:error_setvalue');
    }
};


}, '@VERSION@', {"requires": ["base", "node", "event", "moodle-core_availability-form"]});
