<?php
/**
 * Version info.
 *
 * @package availability_analyticsengineresult
 * @copyright 2023 Digital Learning GmbH
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['conditiontitle'] = 'Analyse Ergebnis';
$string['description'] = 'Kontrollierter Zugriff auf Basis eines Analyse Ergebnis aus Polaris';
$string['error_selectfield'] = 'Es muss eine Analyse-Ergebnis angegeben werden.';
$string['error_setvalue'] = 'You must type a value.';
$string['label_operator'] = 'Method of comparison';
$string['label_value'] = 'Value to compare against';
$string['pluginname'] = 'Beschränkung des Zugriffes durch das Ergebnis einer Analyse von Polaris';
$string['requires_contains'] = 'Your <strong>{$a->field}</strong> contains <strong>{$a->value}</strong>';
$string['requires_doesnotcontain'] = 'Your <strong>{$a->field}</strong> does not contain <strong>{$a->value}</strong>';
$string['requires_endswith'] = 'Your <strong>{$a->field}</strong> ends with <strong>{$a->value}</strong>';
$string['requires_isempty'] = 'Your <strong>{$a->field}</strong> is empty';
$string['requires_isequalto'] = 'Your <strong>{$a->field}</strong> is <strong>{$a->value}</strong>';
$string['requires_isnotempty'] = 'Your <strong>{$a->field}</strong> is not empty';
$string['requires_notendswith'] = 'Your <strong>{$a->field}</strong> does not end with <strong>{$a->value}</strong>';
$string['requires_notisequalto'] = 'Your <strong>{$a->field}</strong> is not <strong>{$a->value}</strong>';
$string['requires_notstartswith'] = 'Your <strong>{$a->field}</strong> does not start with <strong>{$a->value}</strong>';
$string['requires_startswith'] = 'Your <strong>{$a->field}</strong> starts with <strong>{$a->value}</strong>';
$string['missing'] = '(Missing field: {$a})';
$string['title'] = 'Analyse Ergebnis';
$string['op_contains'] = 'beinhaltet';
$string['op_doesnotcontain'] = 'beinhaltet nicht';
$string['op_endswith'] = 'endet mit';
$string['op_isempty'] = 'ist leer';
$string['op_isequalto'] = 'ist identisch zu';
$string['op_isnotempty'] = 'ist nicht leer';
$string['op_startswith'] = 'beginnt mit';
$string['privacy:metadata'] = 'Dieses Plugin speichert keine persönliche Daten der Nutzer.';
